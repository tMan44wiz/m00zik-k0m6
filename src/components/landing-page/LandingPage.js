import React from 'react';
import { MDBBtn, MDBIcon } from 'mdbreact';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { saveArtistAndTracks } from '../../redux/actions/LandingPageAction';

const API_ADDRESS = "https://spotify-api-wrapper.appspot.com";

class LandingPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            artistQuery: "",
            fetchedArtist: null,
            fetchedArtistTracks: [],
            fetchLoading: false,
        };
    }

    updateArtistQuery = (event) => {
        this.setState({
            artistQuery: event.target.value,
        })
    };


    // Check if the "this.state.artistQuery"
    searchForArtist = () => {

        // First clear the "fetchedArtist" in the state
        this.setState({
            fetchedArtist: null,
        });

        return (this.state.artistQuery !== "") ? (
            fetch(`${API_ADDRESS}/artist/${this.state.artistQuery}`)
                .then( (response) => response.json() )
                .then( (fetchedJSON) => {
                    // Check if the Artist total are more than one.
                    if (fetchedJSON.artists.total > 0) {
                        const artist = fetchedJSON.artists.items[0];
                        //this.setState({fetchedArtist: artist});

                        fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
                            .then( (response) => response.json() )
                            .then( (fetchedTracks) => {
                                const topTenTracks = fetchedTracks.tracks;
                                //this.setState({fetchedArtistTracks: topTenTracks, });

                                //  Save All about the Artist to the Redux Store
                                const artistData = [artist, topTenTracks]
                                this.props.saveArtistData(artistData);
                                //console.log(`ARTIST::: ${JSON.stringify(artistData)}`);
                                //console.log(`ARTIST TRACKS::: ${JSON.stringify(topTenTracks)}`);

                            } )
                            .catch( (errorCallback) => { alert(errorCallback.message) } );
                    }
                } )
                .catch((errorCallback) => { alert(errorCallback.message) } )

                // Call the displayArtist() function to change the displayed component
                //this.displayArtist()
        ) : (
            alert("Search box can't be empty.")
        );
    };

    handleKeyPress = (event) => {
        if (event.key === "Enter") {
            this.searchForArtist();
        }
    };




    render() {
        /*const { fetchedArtist } = this.props;
        console.log(`FLAVOUR::: ${fetchedArtist}`);*/
        return (
            <div className= "container-fluid" style= {{ height: `${window.innerHeight}px`, paddingTop: "100px" }}>
                <div className= "">
                    {/*<img src= { Logo } width= "200" alt="Muzik Logo" className= "img-fluid"/>*/}
                    <div className= "mx-auto logoImage"> </div>
                    <h1 className= "font-berkshire-swash font-weight-bold mt-5"  style= {{ fontSize: "4.3em", color: "darkRed" }}>M00zik K0m6</h1>
                </div>

                <div className= "container input-form">
                    <div className= "row">
                        <div className="col-8 mx-auto">
                            <div className="md-form input-group mb-3">
                                <input type="text" className="form-control"
                                       onChange= { this.updateArtistQuery }
                                       onKeyPress= { this.handleKeyPress }
                                       placeholder="Search for Music Artist"
                                       aria-label="Search for Artist"
                                       aria-describedby="MaterialButton-addon2" />
                                <div className="input-group-append">
                                    <MDBBtn onClick= { this.searchForArtist } outline color="primary"  size="sm" type="submit" className="font-weight-bold" style= {{ padding: "13px 20px", borderRadius: 100 }}>
                                        <MDBIcon icon="search" className="mr-1" />
                                        Search
                                    </MDBBtn>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        fetchedArtist: state.artistData.fetchedArtist,
        fetchedArtistTracks: state.artistData.fetchedArtistTracks
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        saveArtistData: (artistData) => {
            dispatch(saveArtistAndTracks(artistData));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LandingPage));