import React from 'react';
import { MDBBtn, MDBIcon, MDBNavbar, MDBNavbarBrand } from "mdbreact";
import { Link } from "react-router-dom";

class AppHeader extends React.Component {

    state = {
        isScrolled: false,
        isOpen: false,
    };

    componentDidMount() {
        window.addEventListener("scroll", this.scrollHandler);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.scrollHandler);
    }

    scrollHandler = () => {
        if (window.scrollY > 20) {
            this.setState({
                isScrolled: true,
            })
        }
        else {
            this.setState({
                isScrolled: false,
            })
        }
    };

    render() {

        const { updateArtistQuery, handleKeyPress, searchForArtist } = this.props;

        return (
            <div className= "container-fluid">
                <MDBNavbar light expand="md" fixed= "top" className= "px-3 px-md-5 py-4 "
                           style= {{
                               backgroundColor: this.state.isScrolled ? ("rgba(25, 25, 25, 1)") : ("transparent"),
                               boxShadow: "none",
                               padding: "10px 5px",
                           }}>
                    <Link to= "/" >
                        <MDBNavbarBrand className= "d-flex">
                            {/*<img src= { Logo } width= "64" alt="App Logo"/>*/}
                            <div className= "mx-auto brandLogo"> </div>
                            <h2 className= "font-berkshire-swash font-weight-bold mt-3 ml-3"
                                style= {{
                                    color: this.state.isScrolled ? ("#ffffff") : ("darkRed"),
                                }}>
                                M00zik K0m6
                            </h2>
                        </MDBNavbarBrand>
                    </Link>
                </MDBNavbar>


                {/* BACK BUTTON and SEARCH INPUT */}
                <div className= "container mx-auto searchFormContainer" style= {{ backgroundColor: this.state.isScrolled ? ("white") : ("white") }}>
                    <div className="row">
                        <div className="col-md-6 text-left">
                            <Link to= "/">
                                <MDBBtn outline color="primary"  size="sm"  className="font-weight-bold mt-4" style= {{ padding: "10px 20px", borderRadius: 100 }}>
                                    <MDBIcon icon="home" className="mr-1" />
                                    Back
                                </MDBBtn>
                            </Link>
                        </div>
                        <div className="col-md-6">
                            <div className="md-form input-group mb-3">
                                <input type="text" className="form-control"
                                       onChange= { updateArtistQuery }
                                       onKeyPress= { handleKeyPress }
                                       placeholder="Search for Artist"
                                       aria-label="Search for Artist"
                                       aria-describedby="MaterialButton-addon2"
                                />
                                <div className="input-group-append">
                                    <MDBBtn onClick= { () => { searchForArtist() } } outline color="primary"  size="sm" type="submit" className="font-weight-bold" style= {{ padding: "13px 20px", borderRadius: 100 }}>
                                        <MDBIcon icon="search" className="mr-1" />
                                        Search
                                    </MDBBtn>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AppHeader;