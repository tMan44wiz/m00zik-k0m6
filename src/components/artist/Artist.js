import React from 'react';
import { MDBBtn } from "mdbreact";

import AppHeader from '../header/AppHeader.js';

class Artist extends React.Component {

    state = {
        imageStyles: {
            maxWidth: 40 + "%",
            borderRadius: 50 + "%",
        }
    };



    render() {
        const { fetchedArtist } = this.props;
        const populateArtistDetails = () => {
            return (fetchedArtist !== null) ? (
                <div>
                    {/*Card*/}
                    <div className= "mb-5 text-center">
                        {/*Card image*/}
                        <div className="view overlay">
                            <img
                                src= { this.props.fetchedArtist.images[0] && this.props.fetchedArtist.images[0].url }
                                className="card-img-top img-fluid mx-auto"
                                style= { this.state.imageStyles }
                                alt= { this.props.fetchedArtist.name + " Image" }
                            />
                        </div>

                        {/*Card content*/}
                        <div className="card-body mt-md-3 mt-lg-4">

                            {/*Title*/}
                            <h4 className="card-title font-merienda h4-responsive font-weight-bold" style= {{ fontSize: "3.2em", color: "darkRed" }}>{ this.props.fetchedArtist.name }</h4>

                            {/*Text*/}
                            <div className="row mt-5 vdivide">
                                {/*Followers*/}
                                <div className="col-6">
                                    <p className="card-text">Followers: <br /> <span style= {{ fontSize: "1.9em", fontWeight: "bold" }}> { this.props.fetchedArtist.followers.total } </span> </p>
                                </div>

                                {/*Popularity*/}
                                <div className="col-6">
                                    <p className="card-text">Popularity: <br /> <span style= {{ fontSize: "1.9em", fontWeight: "bold" }}> { this.props.fetchedArtist.popularity } </span> </p>
                                </div>
                            </div>

                            <hr className= "w-75"/>

                            {/*Genres*/}
                            <div className="row">
                                <div className="col-12">
                                    <h3 className="card-text h3-responsive"> Genres: </h3>
                                    { this.props.fetchedArtist.genres.map( (eachGenres, index) => {
                                        return (
                                            <strong key= { index } style= {{ fontSize: "1.2em", color: "gray" }}>  { eachGenres } <br /> </strong>
                                        )
                                    } ) }
                                </div>
                            </div>

                            {/*Artist Top Ten Tracks Button*/}
                            <div className="row my-5">
                                <div className="col-12">
                                    <MDBBtn
                                        onClick= { this.props.displayArtistTopTenTracks } outline color="primary" type="submit"
                                        className="font-weight-bold"
                                        style= {{ padding: "20px 30px", borderRadius: 100 }}
                                    >
                                        Artist Top Tracks
                                    </MDBBtn>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ) : (
                <div className= "container">
                    <div className="row">
                        <div className="col-12">
                            <i className="fas fa-compact-disc fa-spin fa-7x fa-fw mt-5" style= {{ color: "darkRed" }}> </i>
                        </div>
                    </div>
                </div>
            )
        };
        return (
            <div className= "container-fluid">

                <AppHeader
                    updateArtistQuery= { this.props.updateArtistQuery }
                    handleKeyPress= { this.props.handleKeyPress }
                    searchForArtist= { this.props.searchForArtist }
                />

                <div style= {{ marginTop: "300px" }}>
                    <div className= "row">
                        <div className= "col-md-10 col-lg-8 mx-auto">
                            {/*Card*/}
                            { populateArtistDetails() }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default Artist;
