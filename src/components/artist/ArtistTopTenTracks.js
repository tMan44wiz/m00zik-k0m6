import React from 'react';

import AppHeader from '../header/AppHeader.js';

import PauseLogo from '../../assets/images/pause.png';
import PlayLogo from '../../assets/images/play.png';

class ArtistTopTenTracks extends React.Component {

    state = {
        isPlaying: false,
        currentlyPlayingAudio: null,
        currentlyPlayingURL: null,
        playButton: true,
        currentlyPlayingButton: false,
    };


    playTrack = (trackURL) => () => {        // The double call-Back function is to handle the playTrack(eachTrack.preview_url) function_Call I made in the populateArtistTopTenTracks function below.
        console.log(trackURL);
        const audioTrack = new Audio(trackURL);

        if (!this.state.isPlaying) {
            audioTrack.play();
            this.setState({
                isPlaying: true,
                currentlyPlayingAudio: audioTrack,
                currentlyPlayingURL: trackURL,
            })
        }
        else {
            this.state.currentlyPlayingAudio.pause();

            if (this.state.currentlyPlayingURL === trackURL) {
                this.setState({
                    isPlaying: false,
                })
            }
            else {
                audioTrack.play();
                this.setState({
                    currentlyPlayingAudio: audioTrack,
                    currentlyPlayingURL: trackURL,
                })
            }
        }

    };

    populateArtistTopTenTracks = () => {
        return (this.props.fetchedArtistTracks.length) ? (
            this.props.fetchedArtistTracks.map( (eachTrack) => (
                <div key= { eachTrack.id } className="col-md-4 col-lg-3 text-center">
                    <div className= "card mb-3 py-3">
                        {/*Card image*/}
                        <div
                            className="view overlay mx-auto d-flex align-items-center rounded p-5"
                            onClick= { this.playTrack(eachTrack.preview_url) }
                            style= {{
                                backgroundImage: `url(${eachTrack.album.images[1] && eachTrack.album.images[1].url})`,
                                backgroundPosition: "center",
                                backgroundSize: "cover",
                                backgroundRepeat: "no-repeat",
                                cursor: "pointer",
                            }}
                        >
                            {
                                (eachTrack.preview_url) ? ( (!this.state.playButton) ? (
                                            <img src= { PauseLogo } className= "mx-auto" width= "64" alt="Play/Pause Logo" />
                                    ) : (
                                            <img src= { PlayLogo } className= "mx-auto" width= "64" alt="Play/Pause Logo" />
                                        )
                                ) : (
                                    <h4 className= "h4-responsive grey p-2 text-light font-weight-bold" style= {{ borderRadius: "100px" }}>N/A</h4>
                                )
                            }


                            <div className="mask rgba-white-light"> </div>
                        </div>

                        {/*Card content*/}
                        <div className="card-body">
                            {/*Title*/}
                            <h5 className="card-title font-merienda" style= {{ fontWeight: "bolder", color: "darkRed" }} > { eachTrack.name } </h5>

                            {/*Released Date*/}
                            <p className= "card-text">Released Date:<br /> <span style= {{ fontSize: "1.3em", fontWeight: "bold" }}> { eachTrack.album.release_date } </span></p>
                        </div>
                    </div>
                </div>
            ))
        ) : (
            <div className= "w-100 text-center">
                <p className= "font-merienda font-weight-bold" style= {{ color: "darkRed" }}>
                    <i className="fa fa-spinner fa-pulse fa-3x fa-fw"> </i>
                </p>
            </div>
        )
    };


    render() {
        const { updateArtistQuery, handleKeyPress, searchForArtist } = this.props;

        return (
            <div className= "container-fluid mt-5" style= {{ marginBottom: 100 }}>

                <AppHeader
                    updateArtistQuery= { updateArtistQuery }
                    handleKeyPress= { handleKeyPress }
                    searchForArtist= { searchForArtist }
                />

                <div className="container" style= {{ marginTop: "300px" }}>
                    <div className= "row">
                        {/*Card*/}
                        { this.populateArtistTopTenTracks() }
                    </div>
                </div>
            </div>
        );
    }
};

export default ArtistTopTenTracks;
