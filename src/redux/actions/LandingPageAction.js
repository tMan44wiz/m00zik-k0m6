//  Action Variables.
export const SAVE_ARTIST_AND_TOP_TEN_TRACKS = "SAVE_ARTIST_AND_TOP_TEN_TRACKS";

export const saveArtistAndTracks = (artist) => {
    return {
        type: SAVE_ARTIST_AND_TOP_TEN_TRACKS,
        payload: artist,
    };
};