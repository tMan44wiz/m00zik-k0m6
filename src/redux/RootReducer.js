import { combineReducers } from 'redux';
import { artistDataReducer } from './reducers/LandingPageReducer';

const rootReducer = combineReducers({
    artistData: artistDataReducer,
});

export default rootReducer;