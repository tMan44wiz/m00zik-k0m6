//  Importing Action Type String from the Redux Actions
import { SAVE_ARTIST_AND_TOP_TEN_TRACKS } from '../actions/LandingPageAction';

//  Create the Application Initial State.
const initialArtistState = {
    artistQuery: "",
    fetchedArtist: null,
    fetchedArtistTracks: [],
    fetchLoading: false,
};

export const artistDataReducer = (state = initialArtistState, action) => {
    switch (action.type) {
        case SAVE_ARTIST_AND_TOP_TEN_TRACKS:
            const artist = JSON.stringify(action.payload[0]);
            const topTenTracks = JSON.stringify(action.payload[1]);
            return {
                ...state,
                fetchedArtist: artist,
                fetchedArtistTracks: topTenTracks,
            };

        default: return state;
    }
};